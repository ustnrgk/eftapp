$(document).ready(function () {
    $('.alert .close').click(function () {
        $('.alert').hide();
    });

    $('.starter-template').on('click', '#next-page', function () {
        $('#page-action').remove();
        var pageAction = '<input name="pageAction" type="hidden" value="next" id="page-action">';
        $('#transaction-list-form').append(pageAction);
        $('#list').click();
        window.scrollTo(0, 0);
    });

    $('.starter-template').on('click', '#prev-page', function () {
        $('#page-action').remove();
        var pageAction = '<input name="pageAction" type="hidden" value="prev" id="page-action">';
        $('#transaction-list-form').append(pageAction);
        $('#list').click();
        window.scrollTo(0, 0);
    });

    $('#filter-field').change(function () {
        if ($(this).val() !== '') {
            $('#filter-value').removeAttr('disabled');
        } else {
            $('#filter-value').attr('disabled', true);
        }
    });

    $('#list').click(function (event) {
        event.preventDefault();
        $('.loader').show();
        $('.overlay').show();
        var fromDate = $('#from-date').val();
        var toDate = $('#to-date').val();

        if (fromDate === '' || toDate === '') {
            alert('From date and To date must be filled.');
        } else {
            var formData = $('#transaction-list-form').serialize();
            $.ajax({
                type: 'POST',
                url: "/ajax/transaction/list",
                data: formData,
                success: function (response) {
                    if (response.success === false) {
                        if (response.messages[0] === 'Token Expired') {
                            window.location.href = '/logout';
                        }

                        var messages = '';
                        for (var x = 0; x < response.messages.length; x++) {
                            messages =+ '<li>' + response.messages[x] + '</li>';
                        }
                        $('.alert ul').html(messages);
                        $('.alert').show();
                    } else {
                        var appendHtml;

                        if (!response.data.hasOwnProperty('data')) {
                            window.location.href = '/logout';
                        }

                        if (response.data.data.length === 0) {
                            $('.starter-template #next-page').remove();
                            $('.starter-template #prev-page').remove();

                            if ($('.table-bordered').length > 0) {
                                $('.table-bordered').remove();
                            }

                            if ($('.lead').length > 0) {
                                $('.lead').remove();
                            }
                            appendHtml = '<p class="lead" style="padding-top: 10px;">No results found</p>';
                            $('#transaction-list-form').after(appendHtml);
                            $('.loader').hide();
                            $('.overlay').hide();
                        } else {
                            if ($('.table-bordered').length > 0) {
                                $('.table-bordered').remove();
                            }

                            if ($('.lead').length > 0) {
                                $('.lead').remove();
                            }

                            appendHtml = '<p class="lead" style="padding-top: 10px;">' + response.messages[0] + '</p>';
                            appendHtml = appendHtml.replace('%from%', response.data.from);
                            appendHtml = appendHtml.replace('%to%', response.data.to);
                            appendHtml += '<table class="table table-bordered"></table>';
                            $('#transaction-list-form').after(appendHtml);
                            var tableHeaders = '<thead><tr>';
                            tableHeaders += '<th>Original Amount</th>';
                            tableHeaders += '<th>Original Currency</th>';
                            tableHeaders += '<th>Converted Amount</th>';
                            tableHeaders += '<th>Converted Currency</th>';
                            tableHeaders += '<th>Date Created</th>';
                            tableHeaders += '<th>Date Updated</th>';
                            tableHeaders += '<th>Customer</th>';
                            tableHeaders += '<th>Merchant</th>';
                            tableHeaders += '<th>Transaction</th>';
                            tableHeaders += '<th>Status</th>';
                            tableHeaders += '<th>Payment Method</th>';
                            tableHeaders += '<th>Acquirer</th>';
                            tableHeaders += '<th>Refundable</th>';
                            tableHeaders += '</tr></thead>';

                            var tableBody = '<tbody>';

                            if (response.data.length !== 0) {
                                for (var i = 0; i < response.data.data.length; i++) {
                                    var detailLink = '';
                                    var billingFirstName = '';
                                    var billingLastName = '';
                                    var acquirer = '';
                                    var type = '';
                                    var originalAmount = '';
                                    var originalCurrency = '';
                                    var convertedAmount = '';
                                    var convertedCurrency = '';
                                    var merchantName = '';
                                    var transactionId = '';
                                    var status = '';
                                    var operation = '';

                                    if (response.data.data[i].hasOwnProperty('customerInfo')) {
                                        if (response.data.data[i].customerInfo.hasOwnProperty('billingFirstName')) {
                                            billingFirstName = response.data.data[i].customerInfo.billingFirstName;
                                        }

                                        if (response.data.data[i].customerInfo.hasOwnProperty('billingLastName')) {
                                            billingLastName = response.data.data[i].customerInfo.billingLastName;
                                        }
                                    }

                                    if (response.data.data[i].hasOwnProperty('acquirer')) {
                                        if (typeof response.data.data[i].acquirer === 'object' && response.data.data[i].acquirer !== null) {
                                            acquirer = response.data.data[i].acquirer.name;
                                            type = response.data.data[i].acquirer.type;
                                        } else {
                                            acquirer = response.data.data[i].acquirer;
                                        }
                                    }

                                    if (response.data.data[i].hasOwnProperty('fx')) {
                                        originalAmount = response.data.data[i].fx.merchant.originalAmount;
                                        originalCurrency = response.data.data[i].fx.merchant.originalCurrency;
                                        convertedAmount = response.data.data[i].fx.merchant.convertedAmount;
                                        convertedCurrency = response.data.data[i].fx.merchant.convertedCurrency;
                                    }

                                    if (response.data.data[i].hasOwnProperty('merchant')) {
                                        merchantName = response.data.data[i].merchant.name;
                                    }

                                    if (response.data.data[i].hasOwnProperty('merchant')) {
                                        merchantName = response.data.data[i].merchant.name;
                                    }

                                    if (response.data.data[i].hasOwnProperty('transaction')) {
                                        transactionId = response.data.data[i].transaction.merchant.transactionId;
                                        status = response.data.data[i].transaction.merchant.status;
                                        operation = response.data.data[i].transaction.merchant.operation;
                                        detailLink = '/' + transactionId + '/detail';
                                    }


                                    tableBody += '<tr>';
                                    tableBody += '<td>' + originalAmount + '</td>';
                                    tableBody += '<td>' + originalCurrency + '</td>';
                                    tableBody += '<td>' + convertedAmount + '</td>';
                                    tableBody += '<td>' + convertedCurrency + '</td>';
                                    tableBody += '<td>' + response.data.data[i].created_at + '</td>';
                                    tableBody += '<td>' + response.data.data[i].updated_at + '</td>';
                                    tableBody += '<td><a target="_blank" title="View Details" href="/client' + detailLink + '">' + billingFirstName + ' ' + billingLastName + '</a></td>';
                                    tableBody += '<td><a target="_blank" title="View Details" href="/merchant' + detailLink + '">' + merchantName + '</a></td>';
                                    tableBody += '<td><a target="_blank" title="View Details" href="/transaction' + detailLink + '">' + transactionId  + '</a></td>';
                                    tableBody += '<td>' + status + '</td>';
                                    tableBody += '<td>' + type + '</td>';
                                    tableBody += '<td>' + acquirer + '</td>';
                                    tableBody += '<td>' + response.data.data[i].refundable + '</td>';
                                    tableBody += '</tr>';
                                }

                                tableBody += '</tbody>';

                                var tableData = '';
                                tableData += tableHeaders;
                                tableData += tableBody;

                                $('.table-bordered', '.starter-template').append(tableData);

                                var nextButton = '';
                                var prevButton = '';

                                if (response.data.hasOwnProperty('next_page_url') && response.data.next_page_url !== null && $('#next-page').length === 0) {
                                    if ($('#next-page').length === 0) {
                                        nextButton = '<button class="btn btn-primary" style="margin-left: 2px;" id="next-page">Next</button>';
                                    }
                                } else if (response.data.hasOwnProperty('next_page_url') && response.data.next_page_url === null && $('#next-page').length > 0) {
                                    $('.starter-template #next-page').remove();
                                }

                                if (response.data.hasOwnProperty('prev_page_url') && response.data.prev_page_url !== null && $('#prev-page').length === 0) {
                                    if ($('#prev-page').length === 0) {
                                        prevButton = '<button class="btn btn-primary" style="margin-right: 2px;" id="prev-page">Prev</button>';
                                    }
                                } else if (response.data.hasOwnProperty('prev_page_url') && response.data.prev_page_url === null && $('#prev-page').length > 0) {
                                    $('.starter-template #prev-page').remove();
                                }

                                var buttons = '';
                                buttons += prevButton;
                                buttons += nextButton;

                                if (nextButton.length > 0 && $('#prev-page').length > 0) {
                                    $('.starter-template #prev-page').after(nextButton);
                                } else if (prevButton.length > 0 && $('#next-page').length > 0) {
                                    $('.starter-template #next-page').before(prevButton);
                                } else if (buttons.length !== 0) {
                                    $('.table-bordered', '.starter-template').after(buttons);
                                }

                                if (response.data.hasOwnProperty('next_page_url')) {
                                    $('.starter-template #next-page-url').remove();
                                    var nextPageInput = '';
                                    nextPageInput += '<input type="hidden" name="nextPage" value="' + response.data.next_page_url + '" id="next-page-url">';
                                    $('#transaction-list-form').append(nextPageInput);
                                }

                                if (response.data.hasOwnProperty('prev_page_url')) {
                                    $('.starter-template #prev-page-url').remove();
                                    var prevPageInput = '';
                                    prevPageInput += '<input type="hidden" name="prevPage" value="' + response.data.prev_page_url + '" id="prev-page-url">';
                                    $('#transaction-list-form').append(prevPageInput);
                                }

                                $('.loader').hide();
                                $('.overlay').hide();
                            } else {
                                $('.loader').hide();
                                $('.overlay').hide();
                            }
                        }
                    }
                }
            });
        }
    });
});