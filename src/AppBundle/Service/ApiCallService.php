<?php


namespace AppBundle\Service;

use \GuzzleHttp\Client;

/**
 * Class ApiCallService
 * @package AppBundle\Service
 */
class ApiCallService
{
    /**
     * @var Client
     */
    private $client;

    /**
     * ApiCallService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Sends request and returns response body.
     *
     * @param string $method
     * @param $uri
     * @param array $params
     * @param array $headers
     * @return string
     */
    public function send($uri, $params = array(), $headers = array(), $method = 'POST')
    {
        $response = $this->client->request(
            $method,
            $uri,
            [
                'exceptions' => false,
                'headers' => $headers,
                'json' => $params
            ]
        );

        return $response->getBody()->getContents();
    }
}