<?php

namespace AppBundle\Utility;

use Symfony\Component\HttpFoundation\Response;

/**
 * Maps the ajax responses.
 *
 * Class AjaxResponse
 * @package AppBundle\Utility
 */
class AjaxResponse extends Response
{
    /**
     * AjaxResponse constructor.
     * @param bool $success
     * @param array $data
     * @param array|null $messages
     */
    public function __construct($success = false, $data = [], $messages = null)
    {
        parent::__construct(
            json_encode([
                'success' => $success,
                'data' => $data,
                'messages' => $messages
            ]),
            200,
            ['Content-Type' => 'application/json']
        );
    }
}