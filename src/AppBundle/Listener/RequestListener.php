<?php

namespace AppBundle\Listener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Class RequestListener
 * @package AppBundle\Listener
 */
class RequestListener
{
    /**
     * @param GetResponseEvent $event
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        /**
         * If the request is not a master request
         * or the route is assetic, do nothing.
         */
        if (!$event->isMasterRequest() || strpos($request->get('_route'), '_assetic') !== false) {
            return;
        }

        /**
         * Check if the api_token exists in session.
         * If not, then redirect to homepage.
         */
        if ($request->getSession()->has('api_token')) {
            $now = new \DateTime();
            $tokenExpires = new \DateTime($request->getSession()->get('api_token_expires'));

            /**
             * Check if the api_token expired, is it exists more than 10 minutes.
             * If it exists in the session more than 10 minutes,
             * remove api_token and api_token_expires from session.
             */
            if ($now->diff($tokenExpires)->i > 10) {
                $request->getSession()->remove('api_token');
                $request->getSession()->remove('api_token_expires');

                return $event->setResponse(new RedirectResponse('/'));
            } else {
                if ($request->get('_route') === 'login') {
                    $event->setResponse(new RedirectResponse('/'));
                }
            }

        } else {
            if ($request->get('_route') !== 'login' && $request->get('_route') !== 'homepage') {
                $event->setResponse(new RedirectResponse('/'));
            }
        }

        return $event->getResponse();
    }
}