<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MerchantController
 * @package AppBundle\Controller
 * @Route("/merchant")
 */
class MerchantController extends Controller
{
    /**
     * @param Request $request
     * @param $transactionId
     * @Route("/{transactionId}/detail", name="merchant_detail")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction(Request $request, $transactionId)
    {
        $apiUrl = $this->getParameter('clearsettle_api_url');
        $endpoint = $this->getParameter('clearsettle_endpoints')['merchant'];

        $uri = $apiUrl . $endpoint;
        $params = ['transactionId' => $transactionId];
        $headers = [
            'Authorization' => $request->getSession()->get('api_token')
        ];

        $apiCall = $this->get('app.api_call');

        try {
            $jsonResponse = $apiCall->send($uri, $params, $headers);
            $response = json_decode($jsonResponse, true);

            if (isset($response['merchant'])) {
                return $this->render(
                    ':merchant:detail.html.twig',
                    [
                        'data' => $response,
                        'transactionId' => $transactionId
                    ]
                );
            } else {
                return $this->redirectToRoute('homepage');
            }

        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render(':merchant:detail.html.twig', array('transactionId' => $transactionId));
    }
}
