<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * Handles requests for login and logout processes.
 *
 * Class AccountController
 * @package AppBundle\Controller
 */
class AccountController extends Controller
{
    /**
     * @Route("/login", name="login")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loginAction(Request $request)
    {
        if (!$request->request->has('login')) {
            return $this->redirectToRoute('homepage');
        }

        $email = $request->request->get('email');
        $password = $request->request->get('password');

        $errorMessages = [];
        $validator = $this->get('validator');
        $errors = $validator->validate(
            ['email' => $email, 'password' => $password],
            new Collection([
                'email' => [
                    new Email(['message' => 'Not a valid email.']),
                    new NotBlank(['message' => 'Email cannot be blank.']),
                    new NotNull(['message' => 'Email cannot be null.']),
                ],
                'password' => [
                    new NotBlank(['message' => 'Password cannot be blank.']),
                    new NotNull(['message' => 'Password cannot be null.']),
                ]
            ])
        );

        if (count($errors) > 0) {
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            $this->addFlash('error', $errorMessages);
            return $this->redirectToRoute('homepage');
        }

        $apiUrl = $this->getParameter('clearsettle_api_url');
        $endpoint = $this->getParameter('clearsettle_endpoints')['login'];

        $uri = $apiUrl . $endpoint;
        $params = [
            'email' => $email,
            'password' => $password
        ];

        $apiCall = $this->get('app.api_call');

        try {
            $jsonResponse = $apiCall->send($uri, $params);
            $response = json_decode($jsonResponse, true);
        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
            return $this->redirectToRoute('homepage');
        }

        if (isset($response['status'])) {
            if ($response['status'] === 'APPROVED') {
                $dateTime = new \DateTime();
                $dateTime->modify('+10 minutes');

                $apiToken = $response['token'];
                $apiTokenExpires = $dateTime->format('Y-m-d H:i:s');

                $request->getSession()->set('api_token', $apiToken);
                $request->getSession()->set('api_token_expires', $apiTokenExpires);

            } elseif ($response['status'] === 'DECLINED') {
                $this->addFlash('error', $response['message']);
            }
        } else {
            $this->addFlash('error', 'Something went wrong.');
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/logout", name="logout")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse | Response
     */
    public function logoutAction(Request $request)
    {
        if ($request->getSession()->has('api_token')) {
            $request->getSession()->remove('api_token');
            $request->getSession()->remove('api_token_expires');

            return $this->redirectToRoute('homepage');
        } else {
            return new Response('Page not found', 404);
        }
    }
}
