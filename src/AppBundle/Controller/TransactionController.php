<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * Handles request for transaction operations.
 *
 * Class TransactionController
 * @package AppBundle\Controller
 */
class TransactionController extends Controller
{
    /**
     * @Route("/transaction/report", name="transaction_report")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reportAction(Request $request)
    {
        if ($request->request->has('report')) {
            $fromDate = $request->request->get('fromDate');
            $toDate = $request->request->get('toDate');

            $errorMessages = [];
            $validator = $this->get('validator');
            $errors = $validator->validate(
                ['fromDate' => $fromDate, 'toDate' => $toDate],
                new Collection([
                    'fromDate' => [
                        new Date(['message' => 'From date is not a valid date.']),
                        new NotBlank(['message' => 'From date cannot be blank.']),
                        new NotNull(['message' => 'From date cannot be null.']),
                    ],
                    'toDate' => [
                        new Date(['message' => 'To date is not a valid date.']),
                        new NotBlank(['message' => 'To date cannot be blank.']),
                        new NotNull(['message' => 'To date cannot be null.']),
                    ]
                ])
            );

            if (count($errors) > 0) {
                /** @var ConstraintViolation $error */
                foreach ($errors as $error) {
                    $errorMessages[] = $error->getMessage();
                }

                $this->addFlash('error', $errorMessages);
                return $this->render(':transaction:report.html.twig');
            }

            $apiUrl = $this->getParameter('clearsettle_api_url');
            $endpoint = $this->getParameter('clearsettle_endpoints')['transactions_report'];

            $uri = $apiUrl . $endpoint;
            $params = [
                'fromDate' => $fromDate,
                'toDate' => $toDate
            ];
            $headers = [
                'Authorization' => $request->getSession()->get('api_token')
            ];

            $apiCall = $this->get('app.api_call');

            try {
                $jsonResponse = $apiCall->send($uri, $params, $headers);
                $response = json_decode($jsonResponse, true);

                if (isset($response['status'])) {
                    if ($response['status'] === 'APPROVED') {
                        return $this->render(
                            ':transaction:report.html.twig',
                            [
                                'data' => [
                                    'response' => $response['response'],
                                    'fromDate' => $fromDate,
                                    'toDate' => $toDate
                                ]
                            ]
                        );
                    } elseif ($response['status'] === 'DECLINED') {
                        $this->addFlash('error', $response['message']);
                    }
                } else {
                    $this->addFlash('error', 'Something went wrong.');
                }

            } catch (\Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render(':transaction:report.html.twig');
    }

    /**
     * @Route("/transaction/list", name="transaction_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->render(':transaction:list.html.twig');
    }

    /**
     * @Route("/transaction/{transactionId}/detail", name="transaction_detail")
     * @param Request $request
     * @param $transactionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction(Request $request, $transactionId)
    {
        $apiUrl = $this->getParameter('clearsettle_api_url');
        $endpoint = $this->getParameter('clearsettle_endpoints')['transaction_detail'];

        $uri = $apiUrl . $endpoint;
        $params = ['transactionId' => $transactionId];
        $headers = [
            'Authorization' => $request->getSession()->get('api_token')
        ];

        $apiCall = $this->get('app.api_call');

        try {
            $jsonResponse = $apiCall->send($uri, $params, $headers);
            $response = json_decode($jsonResponse, true);

            if (isset($response['fx'])) {
                return $this->render(
                    ':transaction:detail.html.twig',
                    [
                        'data' => $response,
                        'transactionId' => $transactionId
                    ]
                );
            } else {
                return $this->redirectToRoute('homepage');
            }

        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render(':transaction:detail.html.twig', array('transactionId' => $transactionId));
    }
}
