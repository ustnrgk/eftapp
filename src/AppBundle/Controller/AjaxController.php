<?php

namespace AppBundle\Controller;

use AppBundle\Utility\AjaxResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * Handles the ajax requests.
 *
 * Class AjaxController
 * @package AppBundle\Controller
 * @Route("/ajax")
 */
class AjaxController extends Controller
{
    /**
     * @Route("/transaction/list", name="ajax_transaction_list")
     * @param Request $request
     * @return AjaxResponse
     */
    public function listTransactionAction(Request $request)
    {
        /**
         * Get all params of the request.
         */
        $params = $request->request->all();

        $errorMessages = [];
        $validator = $this->get('validator');

        /**
         * Validate the fromDate and toDate.
         * If there are errors return AjaxResponse with errors.
         */
        $errors = $validator->validate(
            ['fromDate' => $params['fromDate'], 'toDate' => $params['toDate']],
            new Collection([
                'fromDate' => [
                    new Date(['message' => 'From date is not a valid date.']),
                    new NotBlank(['message' => 'From date cannot be blank.']),
                    new NotNull(['message' => 'From date cannot be null.']),
                ],
                'toDate' => [
                    new Date(['message' => 'To date is not a valid date.']),
                    new NotBlank(['message' => 'To date cannot be blank.']),
                    new NotNull(['message' => 'To date cannot be null.']),
                ]
            ])
        );

        if (count($errors) > 0) {
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return new AjaxResponse(false, [], $errorMessages);
        }

        /**
         * Iterate over params and unset empty params.
         */
        foreach ($params as $key => $param) {
            if (empty($param)) {
                unset($params[$key]);
            }
        }

        /**
         * Check if is a uri exists with page parameter from API
         * and request wants to be made to this uri.
         *
         * Otherwise make request without page parameter.
         */
        if (isset($params['pageAction']) && $params['pageAction'] === 'next' && isset($params['nextPage'])) {
            $uri = $params['nextPage'];
        } elseif (isset($params['pageAction']) && $params['pageAction'] === 'prev' && isset($params['prevPage'])) {
            $uri = $params['prevPage'];
        } else {
            $apiUrl = $this->getParameter('clearsettle_api_url');
            $endpoint = $this->getParameter('clearsettle_endpoints')['transaction_list'];

            $uri = $apiUrl . $endpoint;
        }

        $headers = ['Authorization' => $request->getSession()->get('api_token')];

        $apiCall = $this->get('app.api_call');

        try {
            $jsonResponse = $apiCall->send($uri, $params, $headers);
            $response = json_decode($jsonResponse, true);

            if (!isset($response['data'])) {
                if ($response['message'] === 'Token Expired') {
                    return new AjaxResponse(false, [], [$response['message']]);
                } else {
                    return new AjaxResponse(false, [], ['Something went wrong.']);
                }
            }

        } catch (\Exception $e) {
            return new AjaxResponse(false, [], $e->getMessage());
        }

        return new AjaxResponse(true, $response, ['Transaction Results Between %from% To %to%']);
    }
}
